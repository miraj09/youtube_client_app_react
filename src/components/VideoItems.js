import React from 'react';
import { Link } from 'react-router-dom';

function VideoItem(props) {
  const videoInfo = props.item
  return (
    <Link to={{
      pathname: "/watch",
      search: `?v=${videoInfo.id}`,
      state: {
        item: videoInfo,
        id: videoInfo.id
      }
    }}>
      <div
        className="mr-1 mb-6 flex-col inline-block w-screen sm:w-80 md:w-75" click="playVideo">
        <img src={`https://img.youtube.com/vi/${videoInfo.id}/sddefault.jpg`}
          alt="" className="w-full h-40" />
        <div className="flex flex-col relative">
          <div className="ml-8 sm:ml-0 sm:pr-6">
            <h3 className="mt-2 mb-2 text-xl font-medium leading-tight title overflow-hidden">
              {videoInfo.snippet.title}
            </h3>
            <p className="text-xs text-lightGrey2"></p>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default VideoItem;