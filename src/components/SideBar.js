import React from 'react';
import '../assets/css/sidebar.css';
import home from '../assets/icons/home.svg';
import account from '../assets/icons/account.svg';
import sports_soccer from '../assets/icons/sports_soccer.svg';
import add_circle from '../assets/icons/add_circle.svg';
import fire from '../assets/icons/fire.svg';
import subscriptions from '../assets/icons/subscriptions.svg';
import folder from '../assets/icons/folder.svg';
import history from '../assets/icons/history.svg';
import { user, provider } from '../firebase';
import { Link } from 'react-router-dom';

function SideBar(props) {
  const authProvider = provider;
  const signIn = () => {
    // user.createUserWithEmailAndPassword(email, password)
    // .then((userCredential) => {
    //   // Signed in 
    //   const user = userCredential.user;
    //   console.log('>>>>>>auth', user)
    //   // ...
    // })
    // .catch((error) => {
    //   var errorCode = error.code;
    //   var errorMessage = error.message;
    //   // ..
    // });
  // google sign in
    user.signInWithPopup(authProvider)
    .then((result) => {

      // This gives you a Google Access Token. You can use it to access the Google API.
      // The signed-in user info.
      const user = result.user;
      // ...
      // console.log('>>>>>>>>', user)
      props.setAvatar(user.photoURL);
    }).catch(() => {
      // Handle Errors here.
      // The email of the user's account used.
      // The firebase.auth.AuthCredential type that was used.
      // ...
    });
  }

  return (
      <div data-testid="sidebar" id="sidebar" className="app-drawer
        w-64 h-screen fixed inset-y-0 left-0 right-auto transition lg:block shadow">
        <div className="w-full absolute overflow-auto inset-y-0 left-0 pb-30 bg-whiteSmoke">
          <div className="h-full flex flex-col">
            <div className="mt-14"></div>
            <div className="flex flex-1 flex-col">
              <div className="overflow-hidden flex-1">
                <div className="w-60">
                  <div>
                    <div className="border-b border-solid-lightGrey py-3 block">
                      <div>
                        <div className="w-full block">
                            <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                          <Link to="/">
                              <div className="relative w-full min-w-0 h-10 min-h-10
                                flex flex-row items-center px-6 whitespace-no-wrap">
                                <img className="text-brightRed mr-6 inline-flex relative items-center
                                  justify-center align-middle" src={home} alt=""/>
                                <span className="text-base font-medium leading-relaxed
                                  flex-1 overflow-hidden whitespace-no-wrap">Home</span>
                              </div>
                          </Link>
                            </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={fire} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">Trending</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={subscriptions}
                                alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">Subscriptions</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="border-b border-solid-lightGrey py-2 block">
                      <div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={folder} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">Library</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="border-b border-solid-lightGrey inline-block px-8 py-4">
                      <p className="block text-base font-normal leading-normal">
                        Sign in to like videos, comment, and subscribe.
                      </p>
                      <div className="flex">
                        <div className="flex mt-3">
                          <button className="border border-solid-navyBlue px-3 py-3/2 w-auto
                            h-auto flex flex-row items-center justify-center flex-1 bg-transparent" onClick={signIn}>
                            <img className="inline-flex items-center justify-center w-full
                              h-full align-middle relative" src={account.svg} alt=""/>
                            <p className="ml-2 whitespace-no-wrap">SIGN IN</p>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="py-2 border-b border-solid-lightGrey">
                      <h3><span className="px-6 py-2 text-base font-medium">BEST OF YOUTUBE</span></h3>
                      <div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={sports_soccer}
                                alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">Sports</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10
                              flex flex-row items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={history} alt=""/>
                              <span className="text-base font-medium leading-relaxed
                                flex-1 overflow-hidden whitespace-no-wrap">History</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                      <div className="py-2 border-b border-solid-lightGrey">
                        <div className="w-full block">
                          <div className="w-full min-h-10 flex flex-row items-center cursor-pointer">
                            <div className="relative w-full min-w-0 h-10 min-h-10 flex flex-row
                              items-center px-6 whitespace-no-wrap">
                              <img className="text-brightRed mr-6 inline-flex relative items-center
                                justify-center align-middle" src={add_circle}
                                alt=""/>
                            <span className="text-base font-medium leading-relaxed
                              flex-1 overflow-hidden whitespace-no-wrap">Browse channels</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className="pt-4 px-6 pb-0 border-t border-solid-lightGrey">
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">About</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Press</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Copyright</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Contact us</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Creators</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Advertise</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Developers</a>
                      </div>
                      <div className="pt-3 px-6 pb-0">
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Terms</a>
                        <a className="inline-block mr-2 whitespace-no-wrap text-base
                          font-medium no-underline" href="www.google.com">Privacy</a>
                        <a className="inline-block mr-2 whitespace-no-wrap
                          text-base font-medium no-underline"
                           href="www.google.com">Policy & Safety</a>
                        <a className="inline-block mr-2 whitespace-no-wrap
                          text-base font-medium no-underline"
                           href="www.google.com">Test new features</a>
                      </div>
                      <div className="py-2 px-6"></div>
                      <div className="py-4 px-6">
                        <div className="inline text-lightGrey2 text-sm font-normal">
                          © 2019 YouTube, LLC
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}


export default SideBar;