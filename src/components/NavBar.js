/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import "../assets/css/navbar.css";
import menu from "../assets/icons/menu.svg";
import logo from "../assets/images/yt_logo.png";
import back_arrow from "../assets/icons/back-arrow.svg";
import search from "../assets/icons/search.svg";
import { sidebarToggleState, searchResultsState } from "../stateManager.js";

// import account from '../assets/icons/account.svg';

function NavBar() {
  const [searchQuery, setSearchQuery] = useState("");
  const [toggle, setToggle] = useRecoilState(sidebarToggleState);
  const [searchResults, setSearchResults] = useRecoilState(searchResultsState);
  const history = useHistory();

  const toggleSideBar = () => {
    setToggle((toggle) => !toggle);
    console.log("i am toggled", toggle);
  };

  const searchVideos = (e) => {
    e.preventDefault();
    axios({
      method: "get",
      url: "https://www.googleapis.com/youtube/v3/search",
      params: {
        part: "snippet",
        key: process.env.REACT_APP_KEY,
        type: "video",
        maxResults: 5,
        q: searchQuery,
      },
    }).then((response) => {
      setSearchResults(response.data.items);
      sessionStorage.setItem(
        "searchResults",
        JSON.stringify(response.data.items)
      );
      console.log("Items on Navbar component: ", response.data.items);
      history.push("/results")
    });
  };
  // const updateToggle = () => {
  //   setToggle(!toggle)
  // };
  const handleChange = (event) => {
    setSearchQuery(event.target.value);
    // setSearchQuery(event.target.value)
  };
  // const handleSearch = () => {
  //   props.onSearch()
  //   // props.onNavigate()
  // }

  return (
    <div id="navbar" className="w-screen h-16 fixed bg-white top-0 banner">
      <div className="h-14 px-4 justify-between py-0 items-center flex flex-row">
        <div className="sm:inline-block sm:mr-4 p-2 w-10 h-10 relative">
          <button
            data-testid="menu-btn"
            className="align-middle"
            value={toggle}
            onClick={toggleSideBar}
          >
            <img
              className="hidden sm:inline-flex items-center justify-center w-full h-full align-middle relative"
              src={menu}
              alt=""
            />
            <img
              className="sm:hidden inline-flex items-center justify-center w-full h-full align-middle relative"
              src={menu}
              alt=""
            />
          </button>
        </div>
        <Link to="/">
          <div data-testid="home-link" className="w-32 flex flex-row">
            <div className="items-center self-center flex flex-row">
              <div className="w-16 sm:w-20 h-6 inline-flex justify-center items-center">
                <img src={logo} alt="" />
              </div>
            </div>
          </div>
        </Link>
        <div className="flex-1 ml-0 sm:ml-28 xsm:ml-40 sm:mx-10 my-0">
          <form
            className="max-w-dxl md:w-11/12 lg:w-1/2 ml-auto sm:-ml-40 md:ml-0 lg:mx-auto my-0
              flex flex-row flex-1 justify-end relative"
            onSubmit={searchVideos}
          >
            <button className="sm:hidden w-12" click="collapseSearch">
              <img className="w-5" src={back_arrow} alt="" />
            </button>
            <div
              id="container"
              className="relative w-full sm:flex flex-row items-center
                    border border-solid-grey border-r-0
                    rounded-tl-sm bg-white px-3/2 py-1/2 shadow-inner text-black transition"
            >
              <div id="search-input" className="w-full bg-transparent">
                <label htmlFor="searchBox"></label>
                <input
                  type="text"
                  id="searchBox"
                  className="w-full max-w-full leading-normal ml-2 font-normal text-base flex-1 bg-transparent outline-none"
                  placeholder="search"
                  value={searchQuery}
                  onChange={handleChange}
                />
              </div>
            </div>
            <div
              className="border-box absolute border-l border-r border-solid-offWhite
                z-99 top-100 inset-x-0"
              v-show="visible"
            >
              {/* <div
                v-for="(match, index) in matches"
                key="index"
                click="itemClicked(index)"
                className="border-box border-b border-solid-offWhite
                  bg-white p-5/2 cursor-pointer"
              ></div> */}
            </div>
              <button
                data-testid="search-btn"
                className="sm:inline-block w-16 h-full border-none sm:border border-solid-offWhite
                    rounded-tl-none rounded-br-sm bg-white sm:bg-hazy border-l-0"
                onClick={searchVideos}
              >
                <img
                  className="w-5 h-5 text-darkGrey opacity-60 items-center
                     justify-center relative inline-flex"
                  src={search}
                  alt=""
                />
              </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NavBar;
