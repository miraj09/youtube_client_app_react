import React from 'react'

function WatchedList(props) {
	const videoInfo = props.items.item
  return (
    <div className="mt-6 ml-2 mr-4 overflow-hidden h-32">
      <div className="flex flex-col">
        <div className="flex flex-col flex-wrap">
          <div className="flex flex-row inline-block mb-6">
            <div className="flex" click="playVideo">
              <div className="relative inline-block
                              float-left mr-4">
                <img src={videoInfo.snippet.thumbnails.high.url} alt="pic" className="w-54 h-30"/>
              </div>
              <div className="w-7/12">
                <p
                className="h-10 sm:h-12 mb-1 text-base overflow-hidden
                sm:text-xl font-medium leading-tight">
                  {videoInfo.snippet.title}
                </p>
                <div className="flex flex-row mb-2">
                  <p className="mr-4 text-xs">{videoInfo.snippet.description}</p>
                </div>
                <div className="h-8">
                  <p className="text-xs"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default WatchedList