import React from 'react';
// import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import '../assets/css/tailwind.css';
// import VideoItems from './VideoItems.js'

function SearchResults(props) {
  // let location = useLocation();
  // let history = useHistory()
  // useEffect(() => {
  //   let unblock = history.block(tx => {
  //     // Navigation was blocked! Let's show a confirmation dialog
  //     // so the user can decide if they actually want to navigate
  //     // away and discard changes they've made in the current page.
  //     let url = tx.location.pathname;
  //     if (window.confirm(`Are you sure you want to go to ${url}?`)) {
  //       // Unblock the navigation.
  //       unblock();
    
  //       // Retry the transition.
  //       tx.retry();
  //     }
  //   });
  // }, [])
  const videoInfo = props.items
  return (
    <Link to={{
      pathname: "/watch",
      search: `?v=${videoInfo.id.videoId}`,
      state: {
        item: videoInfo,
        id: videoInfo.id.videoId
      }
    }}>
      <div data-testid={videoInfo.id.videoId} className="mt-6 ml-2 mr-4 overflow-hidden h-32">
        <div className="flex flex-col">
          <div className="flex flex-col flex-wrap">
            <div className="flex flex-row mb-6">
              <div className="flex" click="playVideo">
                <div className="relative inline-block
                                float-left mr-4">
                  <img src={videoInfo.snippet.thumbnails.high.url} alt="" className="w-54 h-30"/>
                </div>
                <div className="w-7/12">
                  <p
                  className="h-10 sm:h-12 mb-1 text-base overflow-hidden
                  sm:text-xl font-medium leading-tight">
                    {videoInfo.snippet.title}
                  </p>
                  <div className="flex flex-row mb-2">
                    <p className="mr-4 text-xs">{videoInfo.snippet.description}</p>
                  </div>
                  <div className="h-8">
                    <p className="text-xs"></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}


export default SearchResults;