import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/sidebar.css';
import home from '../assets/icons/home.svg';
import fire from '../assets/icons/fire.svg';
import subscriptions from '../assets/icons/subscriptions.svg';
import folder from '../assets/icons/folder.svg';
import history from '../assets/icons/history.svg';

function CollapsedSideBar(props) {
  return (
    <div data-testid="collapsed-sidebar" id="sidebar" className="hidden fixed left-0 bottom-0 top-14 sm:inline-block bg-whiteSmoke shadow px-4">
      <div className="flex flex-col mt-1">
        <div className="inline-block">
          <Link to="/">
            <div className="flex flex-col items-center justify-center pt-4 pr-0 pb-3 pl-0 w-20">
              <img className="inline-flex items-center justify-center
              relative mb-2" src={home} alt=""/>
              <span className="block max-w-full max-h-6 overflow-hidden
              text-base font-normal whitespace-no-wrap">Home</span>
            </div>
          </Link>
        </div>
        <div className="inline-block">
          <div className="flex flex-col items-center justify-center pt-4 pr-0 pb-3 pl-0 w-20">
            <img className="inline-flex items-center justify-center
            relative mb-2" src={fire} alt=""/>
            <span className="block max-w-full max-h-6 overflow-hidden
            text-base font-normal whitespace-no-wrap">Trending</span>
          </div>
        </div>
        <div className="inline-block">
          <div className="flex flex-col items-center justify-center pt-4 pr-0 pb-3 pl-0 w-20">
            <img className="inline-flex items-center justify-center
            relative mb-2" src={subscriptions} alt=""/>
            <span className="block max-w-full max-h-6
            text-base font-normal whitespace-no-wrap">Subscription</span>
          </div>
        </div>
        <div className="inline-block">
          <div className="flex flex-col items-center justify-center pt-4 pr-0 pb-3 pl-0 w-20">
            <img className="inline-flex items-center justify-center
            relative mb-2" src={folder} alt=""/>
            <span className="block max-w-full max-h-6 overflow-hidden
            text-base font-normal whitespace-no-wrap">Library</span>
          </div>
        </div>
        <div className="inline-block">
          <Link to="/history">
            <div className="flex flex-col items-center justify-center pt-4 pr-0 pb-3 pl-0 w-20">
              <img className="inline-flex items-center justify-center
            relative mb-2" src={history} alt=""/>
              <span className="block max-w-full max-h-6 overflow-hidden
            text-base font-normal whitespace-no-wrap">History</span>
            </div>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default CollapsedSideBar