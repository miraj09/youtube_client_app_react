import React from 'react';
import '../assets/css/tailwind.css';

function VideoPlayer(props) {
  const videoInfo = props.item.snippet
  return (
    <div className="w-80 sm:w-140 h-78 bg-black mx-auto mt-4 sm:my-75">
      <iframe title="{props.id}" className="w-full h-full" src={`https://www.youtube.com/embed/${props.id}`}
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen></iframe>
      <div className='h-56 overflow-hidden'>
        <h2 className="text-2xl mt-4 leading-snug font-semibold">{videoInfo.title}</h2>
        <p className='mt-4 truncate'>{videoInfo.description}</p>
        {/* <p>By nobody</p> */}
      </div>
    </div>
  )
}


export default VideoPlayer;
