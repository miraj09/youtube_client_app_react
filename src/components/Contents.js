/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
// import { Link } from 'react-router-dom';
import axios from "axios";
import "../App.css";
import VideoItem from "./VideoItems.js";
import useIntersectionObserver from "../hooks/intersectionObserver";

// const useIntersectionObserver = () => {
//   const [entry, updateEntry] = useState({})
//   const [node, setNode] = useState(null)

//   const observer = useRef(
//     new IntersectionObserver(([entry]) => updateEntry(entry), {})
//   )
//   useEffect(() => {
//     const { current: currentObserver } = observer
//     currentObserver.disconnect()

//     if (node) currentObserver.observe(node)
//     return () => currentObserver.disconnect()
//   }, [node])
//   return [setNode, entry]
// }

function Contents() {
  const [fetchedItems, setFetchedItems] = useState([]);
  const [pageToken, setPageToken] = useState("");

  const [ref, entry] = useIntersectionObserver({});

  useEffect(() => {
    fetchItems();
  }, []);

  useEffect(() => {
    const loadNewItems = async (token) => {
      const data = await axios({
        method: "get",
        url: "https://www.googleapis.com/youtube/v3/videos",
        params: {
          part: "snippet",
          chart: "mostPopular",
          pageToken: pageToken,
          h1: "en",
          regionCode: "us",
          videoCatagoryId: "20",
          key: process.env.REACT_APP_KEY,
          maxResults: 8,
          Accept: "application/json",
        },
      }).then((response) => {
        console.log("i am new", response);
        const newItems = response.data.items;
        setPageToken(response.data.nextPageToken);
        setFetchedItems((p) => [...p, ...newItems]);
        // setFetchedItems(response.data.items);
      });
      // console.log(data)
    };
    console.log(entry.isIntersecting)
    if (entry.isIntersecting) {
      loadNewItems();
      console.log('from contents', entry)
    }
  }, [entry, pageToken]);

  const fetchItems = async () => {
    const data = await axios({
      method: "get",
      url: "https://www.googleapis.com/youtube/v3/videos",
      params: {
        part: "snippet",
        chart: "mostPopular",
        h1: "en",
        regionCode: "us",
        videoCatagoryId: "20",
        key: process.env.REACT_APP_KEY,
        maxResults: 15,
        Accept: "application/json",
      },
    }).then((response) => {
      console.log(response);
      setPageToken(response.data.nextPageToken);
      setFetchedItems(response.data.items);
    });
    console.log(data);
  };
  // if (callLoader === true) {
  //   loadNewItems()
  // }

  return (
    <div
      data-testid="contents"
      className="content pl-0 lg:px-8 border border-solid-black overflow-hidden"
    >
      <div className="my-6 ml-8">
        <div id="title-container" className="h-8 flex flex-row items-center">
          <h2>
            <span
              className="block max-h-8 leading-loose text-2xl
                          font-medium overflow-hidden"
            >
              Recommended
            </span>
          </h2>
        </div>
      </div>
      {/* <div className="flex">
        <div className="sm:ml-40">
          <div className="flex flex-col">
            <div>
              <div className="border-b border-solid">
                <div className="border-b-0 border-t-0 mt-0 flex flex-col">
                  <div>
                    <div className="mt-6 ml-2">
                      <div
                        id="title-container"
                        className="h-8 flex flex-row items-center"
                      >
                        <h2>
                          <span
                            className="block max-h-8 leading-loose text-2xl
                          font-medium overflow-hidden"
                          >
                            Recommended
                          </span>
                        </h2>
                      </div>
                    </div>
                    <div className="mt-6">
                      <div className="flex flex-col">
                        <div className="flex flex-col sm:flex-row flex-wrap">
                          <div>
                            {fetchedItems.map((item) => (
                              <VideoItem key={item.id} item={item} />
                            ))}
                          </div>
                        </div>
                      </div>
                    </div>
                    <button ref={ref}>Loading More...</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
                            </div> */}
      <div className="grid grid-cols-4 gap-5 min-h-screen">
        <div
          className="mr-1 mb-6 flex-col inline-block w-screen sm:w-80 md:w-75"
          click="playVideo"
        ></div>
        {fetchedItems.map((item) => (
          <VideoItem key={item.id} item={item} />
        ))}
      </div>
      <div id="scrollObserver" ref={ref}></div>
    </div>
  );
}
export default Contents;
