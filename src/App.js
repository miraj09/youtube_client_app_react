import React from "react";
import { useRecoilValue } from "recoil";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./views/Home.js";
import Results from "./views/Results.js";
import Watch from "./views/Watch.js";
import History from "./views/History.js";
import "./assets/css/tailwind.css";
import NavBar from "./components/NavBar.js";
import SideBar from "./components/SideBar.js";
import CollapsedSideBar from "./components/CollapsedSideBar.js";
import { sidebarToggleState } from "./stateManager.js";

function App() {
  const toggleMenu = useRecoilValue(sidebarToggleState);
  // const [ items, setItems ] = useState([])
  // const [ searchQuery, setSearchQuery ] = useState()
  // const [ searchResults, setSearchResults] = useState([])
  // const [ navigateToResults, setNavigateToResults ] = useState(false)

  // useEffect(() => {
  //   fetchItems();
  // }, []);
  // const handleInputChange = searchQuery => {
  //   setSearchQuery(searchQuery)
  // };

  // const fetchItems = async () => {
  //   const data = await axios({
  //       method: 'get',
  //       url: 'https://www.googleapis.com/youtube/v3/videos',
  //       params: {
  //         part: 'snippet',
  //         chart: 'mostPopular',
  //         h1: 'en',
  //         regionCode: 'us',
  //         videoCatagoryId: '20',
  //         key: process.env.REACT_APP_KEY,
  //         maxResults: 15,
  //         Accept: 'application/json',
  //       },
  //     }).then((response) => {
  //       console.log(response);
  //       setItems(response.data.items);
  //     });
  // }

  // const searchVideos = () => {
  //   axios({
  //     method: 'get',
  //     url: 'https://www.googleapis.com/youtube/v3/search',
  //     params: {
  //       part: 'snippet',
  //       key: process.env.REACT_APP_KEY,
  //       type: 'video',
  //       maxResults: 5,
  //       q: searchQuery,
  //     },
  //   })
  //     .then((response) => {
  //       console.log(`Items on Navbar component: ${response.data.items}`)
  //       setSearchResults(response.data.items)
  //   })
  // }
  // // const handleNavigation = () => {
  // //   setNavigateToResults(true)
  // //   console.log(navigateToResults)
  // // }
  // // if(searchResults) {
  // //   return <Redirect to={{
  // //       pathName: '/results',
  // //       state: {
  // //         results: searchResults
  // //       }
  // //     }}
  // //   />
  // // }

  // let sidebar;
  // if (toggle === true) {
  //   sidebar = <CollapsedSideBar />
  // } else {
  //   sidebar = <SideBar />
  // }

  // return (
  //     <Router>
  //       <div className="w-screen h-screen flex">
  //         <RecoilRoot>
  //           <NavBar
  //           searchQuery={searchQuery}
  //           onInputChange={handleInputChange}
  //           onSearch={searchVideos}
  //           />
  //         </RecoilRoot>
  //         <Switch>
  //           <Route exact path="/"><Home items={items} /></Route>
  //           <Route exact path="/results"><Results searchResults={searchResults} /></Route>
  //           <Route exact path="/watch" render={item => <Watch {...item} />} />
  //         </Switch>
  //         {sidebar}
  //       </div>
  //     </Router>
  // );
  // const setAvatar = (avatarUrl) => {
  //   setUserAvatar(avatarUrl)
  // }

  return (
    <Router>
      <main>
        <NavBar />
        {toggleMenu ? <CollapsedSideBar /> : <SideBar />}
        {!toggleMenu ? (
          <div className="absolute left-60 top-14 w-full h-full mask"></div>
        ) : null}
        <div
          data-testid="contents"
          className="content pl-0 lg:px-8 border border-solid-black overflow-hidden"
        >
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/results">
              <Results />
            </Route>
            <Route exact path="/watch">
              <Watch />
            </Route>
            <Route exact path="/history">
              <History />
            </Route>
          </Switch>
        </div>
      </main>
    </Router>
  );
}

export default App;
