import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const firebaseApp = firebase.initializeApp({
  // copy and paste your firebase credential here
  apiKey: process.env.REACT_APP_FIRESTORE,
  authDomain: "client-app-react-4e4bf.firebaseapp.com",
  databaseURL: "https://client-app-react-4e4bf.firebaseio.com",
  projectId: "client-app-react-4e4bf",
  storageBucket: "client-app-react-4e4bf.appspot.com",
  messagingSenderId: "635157359476",
  appId: "1:635157359476:web:d964d3aa031d5d9a8c9a21",
  measurementId: "G-ZDFC1K0Z8Z"
});

const provider = new firebase.auth.GoogleAuthProvider();
const db = firebaseApp.firestore();
const user = firebaseApp.auth();

export {db, user, provider};
