import { atom } from 'recoil'

const items = atom({
	key: 'items',
  default: []
})
const sidebarToggleState = atom({
  key: 'sidebarToggleState',
  default: true
})
const searchResultsState = atom({
	key: 'searchResultsState',
  default: []
})

export { items, sidebarToggleState, searchResultsState }