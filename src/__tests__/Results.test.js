import React from 'react'
import { RecoilRoot } from 'recoil';
import { render, fireEvent, cleanup } from '@testing-library/react'
import axiosMock from 'axios'
// import user from '@testing-library/user-event'
import App from '../App'
import renderWithRouter from '../utils/test-utils'

jest.mock('axios')
afterEach(cleanup)

it('navigates to homepage by clicking logo', () => {
  // const history = createMemoryHistory({initialEntries: ['/results', '/']})
  const {getByTestId, debug} = renderWithRouter(
    <RecoilRoot>
      <App />
    </RecoilRoot>,
    { route: '/results'}
  )
  const homeLink = getByTestId('home-link')
  debug()
  fireEvent.click(homeLink)
  const homePage = getByTestId('home-page')
  expect(homePage).toBeInTheDocument()
  debug()
})