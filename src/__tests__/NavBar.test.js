import React from 'react'
// import {Router} from 'react-router-dom';
// import {createMemoryHistory} from 'history'
// import { Link as MockLink } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import { render, fireEvent, cleanup } from '@testing-library/react'
import user from '@testing-library/user-event'
import axiosMock from 'axios'
import NavBar from '../components/NavBar'
import App from '../App'
import renderWithRouter from '../utils/test-utils'

jest.mock('../axios')

afterEach(cleanup)
// jest.mock('react-router', () => {
//   return {
//     Link: jest.fn(() => null),
//   }
// })

it('renders app bar', () => {
	// const history = createMemoryHistory({initialEntries: ['/']})
  const { queryByPlaceholderText, getByTestId } = renderWithRouter(
    <RecoilRoot>
      <NavBar />
    </RecoilRoot>
  )
  const input = queryByPlaceholderText(/search/i)
  const searchButton = getByTestId('search-btn')
  expect(searchButton).toBeInTheDocument()
  expect(input).toBeInTheDocument()
})

it('toggle navigation drawer by clicking menu-button', () => {
	// const history = createMemoryHistory({initialEntries: ['/']})
  const { getByTestId } = renderWithRouter(
    <RecoilRoot>
      <NavBar />
    </RecoilRoot>
  )
  const menuButton = getByTestId('menu-btn')
  // console.log(menuButton)
  expect(menuButton.value).toBe('false')
  fireEvent.click(menuButton, {target: {value: 'true'}})
  expect(menuButton.value).toBe('true')
  fireEvent.click(menuButton, {target: {value: 'false'}})
  expect(menuButton.value).toBe('false')
  // console.log(user)
})

it('navigates to homepage by clicking logo', () => {
  // const history = createMemoryHistory({initialEntries: ['/results', '/']})
  const {getByTestId, debug} = renderWithRouter(
    <RecoilRoot>
      <App />
    </RecoilRoot>,
    { route: '/results'}
  )
  const homeLink = getByTestId('home-link')
  debug()
  fireEvent.click(homeLink)
  const homePage = getByTestId('home-page')
  expect(homePage).toBeInTheDocument()
  debug()
})

it('route to results page', () => {
  // const history = createMemoryHistory({initialEntries: ['/']})
  const {getByTestId, debug} = renderWithRouter(
    <RecoilRoot>
      <App />
    </RecoilRoot>
  )
  const route = getByTestId('search-btn')
  debug()
  fireEvent.click(route)
  const results = getByTestId('results-page')
  expect(results).toBeInTheDocument()
  debug()
})
