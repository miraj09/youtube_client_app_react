import React from 'react';
// import {Router} from 'react-router-dom';
// import {createMemoryHistory} from 'history'
import { RecoilRoot } from 'recoil';
import { render, fireEvent, cleanup } from '@testing-library/react';
import axiosMock from 'axios'
import renderWithRouter from '../utils/test-utils'
import App from '../App';
import Contents from '../components/Contents'

jest.mock('axios')

afterEach(cleanup)

it('shows sidebar on initial load', () => {
  // const history = createMemoryHistory({initialEntries: ['/']})
	const {getByTestId} = renderWithRouter(
    <RecoilRoot>
      <App />
    </RecoilRoot>
  )
  const sidebar = getByTestId('sidebar')
  expect(sidebar).toBeInTheDocument()
})

it('fetch videos on page load', () => {
  // const history = createMemoryHistory({initialEntries: ['/']})
  axiosMock.get.mockResolvedValue({data: {
      items: [
        {
          snippet: {
            title: "ali"
          }
        },
        {
          snippet: {
            title: "abu"
          }
        }
      ]
    }})
  const url = 'https://www.googleapis.com/youtube/v3/search'
  const {getByTestId, getByLabelText, rerender, debug} = renderWithRouter(
    <RecoilRoot>
      <App />
    </RecoilRoot>
  )
  // debug()
  expect(axiosMock.get).toHaveBeenCalledWith(url)
  expect(axiosMock.get).toHaveBeenCalledTimes(1)
  const contents = getByTestId('contents')
  // expect(results).toBeInTheDocument()
})
