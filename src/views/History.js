import React, { useState, useEffect } from 'react'
import WatchedList from '../components/WatchedList'
import { db } from '../firebase'

function History() {
  const [ customHistory, setCustomHistory ] = useState([])
	useEffect(() => {
    db.collection("watch_history")
    .get()
    .then((querySnapshot) => {
      let arr = []
      querySnapshot.forEach((doc) => {
        arr.push(doc.data())
      })
      setCustomHistory(arr)
    })
  }, [])
  return (
    <div data-testid="results-page" className="bg-snow w-screen h-screen">
      <div className="flex flex-1 mt-14 sm:ml-60 h-screen">
        <div className="flex">
          <div className="w-88 sm:w-81 sm:ml-40">
            <div className="flex flex-col">
              <div>
                <div className="border-b border-solid">
                  <div className="flex flex-col border-b-0 border-t-0 mt-0">
                    <div>
                      <div className="mt-6 ml-2">
                        <div className="h-8 flex flex-row items-center">
                          <h2>
                            <span className="block max-h-8 leading-loose text-2xl 
                            font-medium overflow-hidden">
                             History
                            </span>
                          </h2>
                        </div>
                      </div>
                      <ul>
                       {
                         customHistory ?
                         customHistory.map((e) => <WatchedList key={e.id || e.id.videoId} items={e} />)
                         : <h2>Loading...</h2>
                       }
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default History