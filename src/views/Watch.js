import React, { useEffect } from 'react';
import { useLocation } from 'react-router';
import '../assets/css/tailwind.css';
import VideoPlayer from '../components/VideoPlayer.js';
import { db } from '../firebase'

function Watch(props) {
	
  let customLocation = useLocation();
  const videoInfo = customLocation.state.item
  const videoId = customLocation.state.id
  
  useEffect(() => {
    db.collection("watch_history")
      .add(customLocation.state)
      .then(() => {
        // console.log("Document successfully written!");
    });
  })

  return (
    <div className="w-screen h-screen flex">
      <div className="bg-snow w-screen h-screen">
        <VideoPlayer id={videoId} item={videoInfo} />
        {/* <h1>iframe</h1> */}
      </div>
    </div>
  )
}

// class Watch extends Component {
// 	constructor(props) {
// 		super(props);
// 	}
  
//   render() {
    
    
//   }
// }
export default Watch;