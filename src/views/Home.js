import React from "react";
import "../assets/css/tailwind.css";
import Contents from "../components/Contents.js";

// function useIntersectionApi() {
//   const [ callLoader, setCallLoader ] = useState(false)
//   let bottomBoundary = useRef();

//   useEffect(() => {
//     const observer = new IntersectionObserver(([entry]) => {
//       console.log('iam intersected')
//       if(entry && entry.isIntersecting) {
//         setCallLoader(true)
//       }
//     }, {root: document.querySelector('#contents'), rootMargin: "0px", threshold: 0.1})
//     observer.observe(bottomBoundary.current)
//   }, [])
//   return [bottomBoundary, callLoader]
// }

function Home() {
  // const history = useHistory()
  // const navigateToResults = () => {
  //   history.push('/results', {
  //     results: searchResults
  //   })
  // }
  // const [ fetchedItems, setFetchedItems ] = useState([])
  // const [ pageToken, setPageToken ] = useState()

  // const [ bottomBoundary, callLoader ] = useIntersectionApi()
  // let bottomBoundary = useRef();

  // useEffect(() => {
  //   const observer = new IntersectionObserver(([entry]) => {
  //     console.log(entry)
  //     if(entry && entry.isIntersecting) {
  //       // setCallLoader(true)
  //       console.log('ok')
  //       if (pageToken) {
  //         loadItems()
  //       }
  //     }
  //   }, {root: document.querySelector('#contents'), rootMargin: "0px", threshold: 0.1})
  //   observer.observe(bottomBoundary.current)
  // }, [])

  // useEffect(() => {
  //   fetchItems();
  // }, []);

  // const fetchItems = async () => {
  //     const data = await axios({
  //       method: 'get',
  //       url: 'https://www.googleapis.com/youtube/v3/videos',
  //       params: {
  //         part: 'snippet',
  //         chart: 'mostPopular',
  //         h1: 'en',
  //         regionCode: 'us',
  //         videoCatagoryId: '20',
  //         key: process.env.REACT_APP_KEY,
  //         maxResults: 15,
  //         Accept: 'application/json',
  //       },
  //     })
  //       .then((response) => {
  //       console.log(response);
  //       setPageToken(response.data.pageToken)
  //       setFetchedItems(response.data.items);
  //     });
  //       console.log(data)
  // }
  // // if (callLoader === true) {
  // //   loadItems()
  // // }
  // const loadItems = async () => {
  //     const data = await axios({
  //       method: 'get',
  //       url: 'https://www.googleapis.com/youtube/v3/videos',
  //       params: {
  //         part: 'snippet',
  //         chart: 'mostPopular',
  //         pageToken: pageToken,
  //         h1: 'en',
  //         regionCode: 'us',
  //         videoCatagoryId: '20',
  //         key: process.env.REACT_APP_KEY,
  //         maxResults: 5,
  //         Accept: 'application/json',
  //       },
  //     })
  //       .then((response) => {
  //       console.log('i am new', response);
  //       const newItems = response.data.items
  //       setFetchedItems((p) => [...p, ...newItems])
  //       // setFetchedItems(response.data.items);
  //     });
  //       // console.log(data)
  // }
  // const searchVideos = () => {
  //   // e.preventDefault();
  //   history.push('/results')
  //   console.log('i am clicked.');
  //   axios({
  //     method: 'get',
  //     url: 'https://www.googleapis.com/youtube/v3/search',
  //     params: {
  //       part: 'snippet',
  //       key: process.env.REACT_APP_KEY,
  //       type: 'video',
  //       maxResults: 5,
  //       q: searchQuery,
  //     },
  //   })
  //     .then((response) => {
  //       console.log(response.data.items)
  //       setSearchResults(response.data.items)
  //       // navigateToResults()
  //   })
  // };

  return <Contents />;
}

// class Home extends Component {
//   constructor(props) {
//     super(props);
//     this.handleInputChange = this.handleInputChange.bind(this);
//     this.searchVideos = this.searchVideos.bind(this);
//     this.updateToggle = this.updateToggle.bind(this);
//     this.state = {
//       searchQuery: '',
//       searchResults: null,
//       toggle: false
//     };
//   }

//   render() {
//     const searchQuery = this.state.searchQuery;
//     const toggle = this.state.toggle;

//   }
// }

export default Home;
