import React, { useState, useEffect } from 'react';
import { useRecoilState } from 'recoil';
import '../assets/css/tailwind.css';
import SearchResults from '../components/SearchResults.js';
import { searchResultsState } from '../stateManager.js';

function Results(props) {
  const [ searchResults ] = useRecoilState(searchResultsState)
  const [ sessionStorageData, setSessionStorageData ] = useState('')
  useEffect(() => {
    setDataToStore()
  }, [searchResults])
  const setDataToStore = () => {
    const storedData = JSON.parse(sessionStorage.getItem('searchResults'))
    setSessionStorageData(storedData)
  }

  return (
    <div data-testid="results-page" className="bg-snow w-screen h-screen">
      <div className="flex flex-1 mt-14 sm:ml-60 h-screen">
        <div className="flex">
          <div className="w-88 sm:w-81 sm:ml-40">
            <div className="flex flex-col">
              <div>
                <div className="border-b border-solid">
                  <div className="flex flex-col border-b-0 border-t-0 mt-0">
                    <div>
                      <div className="mt-6 ml-2">
                        <div className="h-8 flex flex-row items-center">
                          <h2>
                            <span className="block max-h-8 leading-loose text-2xl
                            font-medium overflow-hidden">
                             Filter
                            </span>
                          </h2>
                        </div>
                      </div>
                      <ul>
                      { sessionStorageData ? sessionStorageData.map(eachItem => <SearchResults key={eachItem.id.videoId} items={eachItem}/>) : <h2>Loading</h2> }
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

// class Results extends Component {
// 	constructor(props) {
// 		super(props);
//     this.handleInputChange = this.handleInputChange.bind(this);
//     this.searchVideos = this.searchVideos.bind(this);
//     this.state = {
//       searchQuery: this.props.location.state.q,
//       searchResults: null,
//     };
// 	}
//   handleInputChange(searchQuery) {
//     this.setState({searchQuery})
//   }
//   componentDidMount() {
//     if (this.state.searchQuery) {
//       return this.searchVideos()
//     }
//   }
//   searchVideos() {
//     // e.preventDefault();
//     console.log('i am clicked.');
//     axios({
//         method: 'get',
//         url: 'https://www.googleapis.com/youtube/v3/search',
//         params: {
//           part: 'snippet',
//           key: process.env.REACT_APP_KEY,
//           type: 'video',
//           maxResults: 5,
//           q: this.state.searchQuery,
//         },
//       })
//         .then((response) => {
//           console.log(response.data.items)
//           this.setState(() => {
//             return {searchResults: response.data.items}
//           })
//       })
//     }
//   render() {
//     const searchQuery = this.state.searchQuery

//     let elem;
//     if(this.state.searchResults) {
//       elem = <Items items={this.state.searchResults} />;
//     }

//     return (
//       <div className="w-screen h-screen flex">
//         <div className="bg-snow w-screen h-screen">
//           <NavBar
//             searchQuery={searchQuery}
//             onInputChange={this.handleInputChange}
//             onSearch={this.searchVideos} />
//           <div className="flex flex-1 mt-14 sm:ml-60 h-screen">
//             <div className="flex">
//               <div className="w-88 sm:w-81 sm:ml-40">
//                 <div className="flex flex-col">
//                   <div>
//                     <div className="border-b border-solid">
//                       <div className="flex flex-col border-b-0 border-t-0 mt-0">
//                         <div>
//                           <div className="mt-6 ml-2">
//                             <div className="h-8 flex flex-row items-center">
//                               <h2>
//                                 <span className="block max-h-8 leading-loose text-2xl
//                                 font-medium overflow-hidden">
//                                  Filter
//                                 </span>
//                               </h2>
//                             </div>
//                           </div>
//                           <ul>
//                             {/* this.state.searchResults && <Items items={this.state.searchResults} />; */}
//                             {elem}
//                           </ul>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//         <SideBar />
//       </div>
//     )
//   }
// }
export default Results
