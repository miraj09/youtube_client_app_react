const { colors: { blue, green, red, teal, pink, purple, orange, indigo, yellow, ...colors } } = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [
    './src/**/*.js',
  ],  
  corePlugins: {
  },
  theme: {
    colors: colors,
    zIndex: {
      '99': 99,
    },
  	extend: {
  		spacing: {
        '1/2': '.125rem',
        '3/2': '.375rem',
        '5/2': '.625rem',
        '09': '2.25rem',
        '14': '3.5rem',
        '28': '7rem',
        '30': '7.5rem',
        '52': '13rem',
        '54': '13.125rem',
        '60': '15rem',
        '68': '17rem',
        '75': '18.75rem',
        '78': '19.5rem',
        '80': '20rem',
        '88': '22rem',
        '81': '80.25rem',
        '104': '26rem',
        '140': '35rem',
      },
      colors: {
        'snow': '#fafafa',
        'whiteSmoke': '#f5f5f5',
        'grey': '#cccccc',
        'black': '#111111',
        'offWhite': '#d3d3d3',
        'hazy': '#f8f8f8',
        'darkGrey': '#333333',
        'navyBlue': '#065fd4',
        'lightGrey': '#0000001a',
        'lightGrey2': '#909090',
        'brightRed': '#ff0000',
        'ash': '#888888'
      },
      maxWidth: {
        'dxl': '40rem'
      },
      boxShadow: {
        'inner': 'inset 0 1px 2px #eee'
      },
      inset: {
        '2': '.50rem',
        '3': '.750rem',
        '11': '2.75rem',
        '14': '3.5rem',
        '60': '15rem',
        '65': '-65',
        '100': '100%',
        '120': '-120',
        '322': '89.5rem'
      },
      opacity: {
        '60': '.6'
      },
      fontSize: {
        '3xl': '1.4rem'
      },
      screens: {
        'xsm': '410px'
      }
  	}
  },
  variants: {
    /* 
    appearance: [],
    borderColor: [],
    backgroundColor: [],
    display: [],
    outline: [],
    zIndex: [],
    boxShadow: [],
    textColor: [],
    */
  },
  plugins: []
}

